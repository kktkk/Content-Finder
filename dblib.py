#!/usr/bin/env python

import sqlite3

conn = sqlite3.connect("ContentFinder.db")
c = conn.cursor()


class Keywords:

    def add_keyword(self, word):
        c.execute("insert into keywords word=?", word)
        print "keyword: %s added." % word


    def del_keyword(self, word):
        c.execute("delete from keywords where word=?", word)
        print "keyword: %s deleted." % word

    def __init__(self, add_keyword, del_keyword):
        self.add_keyword = add_keyword()
        self.del_keyword = del_keyword()


class Seachengines:

    def add_searchengine(self, args):
        for name, url, depth in args:
            c.execute("insert into searchengines values(?,?,?)", (name, url, depth))
            print "SearchEngine: %s added." % name

    def del_searchengine(self,args):
        for name in args:
            c.execute("delete from searchengines where name=?", name)
            print "SearchEngine: %s deleted" % name

    def show_all(self):
        c.execute("select * from searchengines")
        c.fetchall()
        print "Show all searchengines"

    def show_searcengine(self, args):
        for name in args:
            c.execute("select * from searchengine where name=?", name)
            c.fetchone()
            print "Show searchine: %s" % name

    def __init__(self, args):
        self.add_searchengine(self, args)
        self.del_searchengine(self, args)
        self.show_all(self, args)
        self.show_searcengine(self, args)

class Websites:

    def add_website(self, args):
        for name, url in args:
            c.execute("insert into websites values(?,?)", (name, url,))
            print "Website: %s added." % name

    def del_website(selfself, args):
        for name in args:
            c.execute("delete from websites where=?", name)
            print "Website: %s deleted."

    def __init__(self, add_website, del_website):
        self.add_website = add_website()
        self.del_website = del_website()

class Results:

    def add_results(self, args):
        for date, title, content, keyword_id, website_id in args:
            c.execute("insert into results values(?,?,?,?,?)", (
                date,
                title,
                content,
                keyword_id,
                website_id,))
            print "Results added to database."

    def del_results(self, args):
        for title in args:
            c.execute("delete from results where title=?", title)
            print "Results: %s deleted." % title

    def __init__(self, add_results, del_results):
        self.add_results = add_results()
        self.del_results = del_results()


class Links:

    def add_links(self, args):
        for result_id, name, url in args:
            c.execute("insert into links values(?,?,?)", (result_id, name, url,))
            print "Link: %s added." % name

    def del_links(self, args):
        for name in args:
            c.execute("delete from links where name=?", name)
            print "Link: %s deleted." % name

    def __init__(self, add_links, del_links):
        self.add_links = add_links()
        self.del_links = del_links()