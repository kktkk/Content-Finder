#!/usr/bin/env python

import sqlite3

Error = "Problem creating db!"
db_file = "ContentFinder.db"


def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return None


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def main():

    keywords_table = """ CREATE TABLE IF NOT EXISTS keywords (
        id integer PRIMARY KEY,
        word text NOT NULL
        ); """

    searchengines_table = """CREATE TABLE IF NOT EXISTS searchengines (
        id integer PRIMARY KEY,
        name text NOT NULL,
        url text NOT NULL,
        depth integer NOT NULL
        );"""

    websites_table = """CREATE TABLE IF NOT EXISTS websites (
        id integer PRIMARY KEY,
        name text NOT NULL,
        url text NOT NULL
        );"""

    results_table = """CREATE TABLE IF NOT EXISTS results (
        id integer PRIMARY KEY,
        date text NOT NULL,
        title text NOT NULL,
        content text  NOT NULL,
        keyword_id integer NOT NULL,
        website_id integer NOT NULL,
        FOREIGN KEY (keyword_id) REFERENCES keywords (id),
        FOREIGN KEY (website_id) REFERENCES website (id)
        );"""

    links_table = """CREATE TABLE IF NOT EXISTS links (
        id integer PRIMARY KEY,
        result_id integer NOT NULL,
        name text,
        url text  NOT NULL,
        FOREIGN KEY (result_id) REFERENCES results (id)
        );"""

    # create a database connection
    conn = create_connection(db_file)
    if conn is not None:
        # create tables
        create_table(conn,keywords_table)
        create_table(conn, searchengines_table)
        create_table(conn, websites_table)
        create_table(conn, results_table)
        create_table(conn, links_table)
    else:
        print("Error! cannot create the database connection.")

if __name__ == '__main__':
    main()
