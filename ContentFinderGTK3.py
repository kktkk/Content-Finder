#!/usr/bin/env python

import gi
import urllib
from HTMLParser import HTMLParser
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

links = []
content = []


# Custom HTML Parser
class MyHTMLParser(HTMLParser):
    """Custom html link parser"""

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for name, value in attrs:
                if name == "href":
                    links.extend([value])

    def handle_data(self, data):
        if data != "":
            content.extend([data])


# instantiate the parser and fed it some HTML
parser = MyHTMLParser()


class ContentFinder:
    """Content Finder Main class"""

    def __init__(self):
        self.builder = Gtk.Builder()  # create an instance of the gtk.Builder
        self.builder.add_from_file("ContentFinder.glade")  # add the xml file to the Builder
        self.window = self.builder.get_object("MainWindow")  # This gets the 'window1' object
        self.aboutdialog = self.builder.get_object("aboutdialog")
        self.statusbar = self.builder.get_object("statusbar")
        self.context_id = self.statusbar.get_context_id("status")
        self.keyword = self.builder.get_object("entry_keywords")
        self.website = self.builder.get_object("entry_website")
        self.result = self.builder.get_object("results")
        self.status_count = 0
        self.builder.connect_signals(self)
        self.window.show()

    """"Kill/Quit events for the Main Window"""

    def on_MainWindow_destroy(self, object, data=None):
        print "quit with cancel"
        Gtk.main_quit()

    def on_gtk_quit_activate(self, menuitem, data=None):
        print "quit from menu"
        Gtk.main_quit()

    """About event handler"""

    def on_gtk_about_activate(self, menuitem, data=None):
        print "help about selected"
        self.response = self.aboutdialog.run()
        self.aboutdialog.hide()

    """Push Status Function"""

    def on_push_status_activate(self, menuitem, data=None):
        self.status_count += 1
        self.statusbar.push(self.context_id, "Message number %s" % str(self.status_count))

    """Pop Status Function"""

    def on_pop_status_activate(self, menuitem, data=None):
        self.status_count -= 1
        self.statusbar.pop(self.context_id)

    """Clear Status Function"""

    def on_clear_status_activate(self, menuitem, data=None):
        while (self.status_count > 0):
            self.statusbar.pop(self.context_id)
            self.status_count -= 1

    """Search"""

    def on_search_clicked(self, widget, data=None):
        print "Search initiated."
        url = str(self.website.get_text())
        keyword = str(self.keyword.get_text())
        page = urllib.urlopen(url)
        parser.feed(page.read())

        def count_links(links):
            return "Links: " + str(len(links))

        def count_keywords_in_content(keyword):
            return "Keywords: " + str(content.count(keyword)) + "\n"

        def show_links(links):
            n = len(links)
            dic = {}
            for x in range(n):
                dic[x + 1] = links[x - 1]
            return "Links List: " + "\n" + str(dic) + "\n"

        def show_content(content):
            n = len(content)
            dic = {}
            for t in ("\n", "\t", "\n\t", "\t\n"):
                try:
                    content[:].remove(t[:])
                except:
                    pass
            for x in range(n):
                dic[x + 1] = content[x - 1]
            if dic.values() == t:
                dic.pop(dic.key)
            return "Content: " + "\n"  + str(dic)


        data = "Stats: \n%s \n%s \n%s \n%s" % (
            count_links(links),
            count_keywords_in_content(keyword),
            show_links(links),
            show_content(content)
        )

        self.result.set_text(data)


if __name__ == "__main__":
    main = ContentFinder()  # create an instance of our class
    Gtk.main()  # run the darn thing
